import { Component } from "react";
import "../CardDeck.css"
import ProductData from "../../../ProductData.json";

class CardDeck3 extends Component {
    render() {
        return (
            <>
                <div className="div-card-row">
                    {/* Product 7 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[6].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[6].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[6].Description).substring(0, 100)} {(ProductData.Products[6].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[6].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[6].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[6].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[6].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 8 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[7].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[7].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[7].Description).substring(0, 100)} {(ProductData.Products[7].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[7].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[7].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[7].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[7].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 9 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[8].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[8].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[8].Description).substring(0, 100)} {(ProductData.Products[8].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[8].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[8].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[8].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[8].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>

                </div>
            </>
        )
    }
}

export default CardDeck3;