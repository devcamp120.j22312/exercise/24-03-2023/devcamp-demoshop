import { Component } from "react";
import "../CardDeck.css"
import ProductData from "../../../ProductData.json";

class CardDeck2 extends Component {
    render() {
        return (
            <>
                <div className="div-card-row">
                    {/* Product 4 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[3].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[3].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[3].Description).substring(0, 100)} {(ProductData.Products[3].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[3].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[3].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[3].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[3].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 5 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[4].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[4].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[4].Description).substring(0, 100)} {(ProductData.Products[4].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[4].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[4].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[4].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[4].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 6 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[5].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[5].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[5].Description).substring(0, 100)} {(ProductData.Products[5].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[5].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[5].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[5].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[5].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>

                </div>
            </>
        )
    }
}

export default CardDeck2;