import { Component } from "react";
import "../CardDeck.css"
import ProductData from "../../../ProductData.json";

class CardDeck1 extends Component {
    render() {
        return (
            <>
                <div className="mt-3">
                    <h3>Product List</h3>
                    <h6>Showing 1 - 9 of 24 products</h6>
                </div>
                <div className="div-card-row">
                    {/* Product 1 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[0].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[0].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[0].Description).substring(0, 100)} {(ProductData.Products[0].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[0].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[0].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[0].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[0].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 2 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[1].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[1].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[1].Description).substring(0, 100)} {(ProductData.Products[1].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[1].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[1].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[1].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[1].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>
                    {/* Product 3 */}

                    <div className="card div-card-product">
                        <div className="card-header">
                            <h3 className="text-center text-primary">
                                {ProductData.Products[2].Title}
                            </h3>
                        </div>
                        <div className="div-image">
                            <img className="card-image" src={ProductData.Products[2].ImageUrl} />
                        </div>
                        <div className="card-body">
                            <p>{(ProductData.Products[2].Description).substring(0, 100)} {(ProductData.Products[2].Description).length >= 100 && '...'}</p>
                            <div className="mt-2">
                                <b>Category: </b> {ProductData.Products[2].Category}
                            </div>
                            <div className="mt-2">
                                <b>Made by: </b> {ProductData.Products[2].Manufacturer}
                            </div>
                            <div className="mt-2">
                                <b>Organic: </b> {ProductData.Products[2].Organic = true ? "Yes" : "No"}
                            </div>
                            <div className="mt-2">
                                <b>Price: </b> {ProductData.Products[2].Price}
                            </div>
                            <div className="div-btn">
                                <button className="btn btn-primary">ADD TO CART</button>
                            </div>

                        </div>
                    </div>

                </div>
            </>
        )
    }
}

export default CardDeck1;