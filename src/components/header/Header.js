import { Component } from "react";
import logo from "../../assets/images/logo.png"

class Header extends Component {
    render() {
        return (
            <>
                <div className="div-header">
                    <div>
                        <img className="logo-header" src={logo} alt="logo" />
                    </div>
                    <div className="div-header-title">
                        <h1 className="text-center">React Store</h1>
                        <h6 className="text-center">Demo App Shop24h v1.0</h6>
                    </div>
                </div>

                <div className="div-navbar">
                    <div className="text-navbar">
                        Home
                    </div>
                </div>
            </>
        )
    }
}
export default Header;