import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"

import CardDeck1 from "./components/content/cardDeck1/CardDeck1";
import CardDeck2 from "./components/content/cardDeck2/CardDeck2";
import CardDeck3 from "./components/content/cardDeck3/CardDeck3";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";

function App() {
  return (
    <div className="container-fluid">
      <Header />

      <div className="container">
        <CardDeck1 />
        <CardDeck2 />
        <CardDeck3 />
      </div>

      <Footer />
    </div>
  );
}

export default App;
